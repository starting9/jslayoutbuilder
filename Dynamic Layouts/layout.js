let layoutStructure = {
    'header': true,
    'footer': true,
    'columnNo': 3,
    'columns': ['nav', 'article', 'aside'],
    'columnSequence': ['nav', 'article', 'aside']
};

const Output = document.getElementById('output');
Output.setAttribute('style', 'display: grid; grid-template-columns: auto auto auto auto; grid-gap: 10px; padding: 10px;');

Pondit = {};

Pondit.Layout = {

    header: () => {
        d1 = document.createElement('div');
        d1.setAttribute('id', 'header')
        if (layoutStructure.columnSequence.includes('nav')) 
        {
            d1.setAttribute('style', 'background-color: lime; grid-area: 1/2/span 5/span 3');
        }
        else {
            d1.setAttribute('style', 'background-color: lime; grid-area: 1/1/span 5/span 4');
        }

        text = document.createTextNode('Header');

        d1.appendChild(text);

        return d1;
    },

    footer: () => {
        d1 = document.createElement('div');
        d1.setAttribute('id', 'footer')
        d1.setAttribute('style', 'background-color: cyan; grid-area: 26/1/span 5/span 4');

        text = document.createTextNode('Footer');

        d1.appendChild(text);

        return d1;
    },

    nav: () => {
        d1 = document.createElement('div');
        d1.setAttribute('id', 'nav');
        d1.setAttribute('style', 'background-color: cyan; grid-area: 1/1/span 25/span 1;');

        text = document.createTextNode('Navbar');

        d1.appendChild(text);

        return d1;
    },

    article: () => {
        d1 = document.createElement('div');
        d1.setAttribute('id', 'article')

        serial = layoutStructure.columnSequence.indexOf('article');
        if (layoutStructure.columnSequence.includes('nav'))
        {
            d1.setAttribute('style', 'background-color: green; grid-area: 6/' + parseInt(serial + 1) + '/span 20/span 2;');
        }
        else
        {
            if(layoutStructure.columnSequence.indexOf('article') < layoutStructure.columnSequence.indexOf('aside'))
            {
                d1.setAttribute('style', 'background-color: green; grid-area: 6/' + parseInt(serial + 1) +'/span 20/span 3;');
            }
            else
            {
                d1.setAttribute('style', 'background-color: green; grid-area: 6/' + parseInt(serial + 1) +'/span 20/span 3;');
            }
        }


        text = document.createTextNode('Article');

        d1.appendChild(text);

        return d1;
    },

    aside: () => {
        d1 = document.createElement('div');
        d1.setAttribute('id', 'aside');
        serial = layoutStructure.columnSequence.indexOf('aside');
        if (layoutStructure.columnSequence.includes('nav'))
        {
            d1.setAttribute('style', 'background-color: cyan; grid-area: 6/' + parseInt(serial + 2) + '/span 20/span 1;');
        }
        else
        {
            if(layoutStructure.columnSequence.indexOf('article') < layoutStructure.columnSequence.indexOf('aside'))
            {
                d1.setAttribute('style', 'background-color: cyan; grid-area: 6/' + parseInt(serial + 3) + '/span 20/span 1;');
            }
            else
            {
                d1.setAttribute('style', 'background-color: cyan; grid-area: 6/' + parseInt(serial + 1) + '/span 20/span 1;');
            }
        }
        text = document.createTextNode('Aside');

        d1.appendChild(text);

        return d1;
    },

    build: () => {
        if (layoutStructure.header) {
            Output.appendChild(Pondit.Layout.header());
        }

        if (layoutStructure.columnSequence.includes('nav')) {
            Output.appendChild(Pondit.Layout.nav());
        }

        if (layoutStructure.columnSequence.includes('article')) {
            Output.appendChild(Pondit.Layout.article());
        }

        if (layoutStructure.columnSequence.includes('aside')) {
            Output.appendChild(Pondit.Layout.aside());
        }

        if (layoutStructure.footer) {
            Output.appendChild(Pondit.Layout.footer());
        }
    },

    html: () => {
        return document.getElementsByTagName('body')[0].firstElementChild.outerHTML;
    }
}

Pondit.Layout.build();
console.log(Pondit.Layout.html());
